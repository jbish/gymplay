from PIL import Image
import collections
import numpy as np

IMG_WIDTH = 84
IMG_HEIGHT = 84

class Preprocessor():

    # In the context of this class, a "frame" is an observation given by the
    # emulator, which is just an image

    def __init__(self, phi_length, compwise_max_flag, crop_flag, env_name):
        self.phi_length = phi_length
        # frames just before the actual seen frames:
        # used to calc the pairwise max over 2 consec. frames
        self.compare_frame_buffer = collections.deque(maxlen=self.phi_length) 
        # actual seen frames, the ones not skipped.
        self.seen_frame_buffer = collections.deque(maxlen=self.phi_length)
        self.compwise_max_flag = compwise_max_flag
        self.crop_flag = crop_flag
        self.env_name = env_name
    
    def try_store_frame(self, frame_count, frameskip, obs):
        if frame_count % frameskip == 0:
            self.add_seen_frame(obs)
        elif frame_count % frameskip == frameskip-1:
            self.add_compare_frame(obs)

    def add_compare_frame(self, frame):
        self.compare_frame_buffer.append(frame)

    def add_seen_frame(self, frame):
        self.seen_frame_buffer.append(frame)

    def preprocess_frame(self, frame):
        # Game frames are 210*160*3 arrays
        # Preprocessing a frame involves:
        #   1. Converting to grayscale
        #   2. Optionally cropping
        #   3. Rescale to IMG_HEIGHT*IMG_HEIGHT pixels.
        # NOTE: After these steps, the frame data is still integers within the
        # range 0 - 255.
        img = Image.fromarray(frame)
        img = img.convert("L")
        if self.crop_flag:
            img = self.crop_frame(img)
        preprocessed_img = img.resize((IMG_HEIGHT, IMG_WIDTH), resample=Image.BILINEAR)
        # get data from preprocessed img and reshape it to 
        # IMG_HEIGHT*IMG_HEIGHT ndarray with
        # dtype of uint8 - the img construction above will give a flattened
        # array when getting its data
        preprocessed_img_data = np.asarray(preprocessed_img.getdata(),
                dtype=np.uint8)
        preprocessed_img_data = np.reshape(preprocessed_img_data,
                (IMG_HEIGHT, IMG_WIDTH))
        return preprocessed_img_data

    def compwise_max(self, compare_frame, seen_frame):
        # Does the component-wise maximum over two frames as in the paper.
        # The frames in this cases are expected to have 1 colour channel (they
        # are grayscale, having already been preprocessed).
        return np.maximum(compare_frame, seen_frame)

    def create_phi(self):
        # Phi function from the paper:
        # Loops over the two frame buffers and optionally do pairwise processing to
        # determine max activation for each pixel, returning processed frames
        # as a stack of frames with length equal to self.phi_length.
        phi = np.empty(shape=(self.phi_length, IMG_HEIGHT, IMG_WIDTH), dtype=np.uint8)
        i = 0
        for tup in zip(self.compare_frame_buffer, self.seen_frame_buffer):
            compare_frame = tup[0]
            seen_frame = tup[1]
            res = None
            if self.compwise_max_flag:
                res = self.compwise_max(compare_frame, seen_frame)
            else:
                res = seen_frame
            res = self.preprocess_frame(res)
            phi[i] = res
            i += 1
        return phi

    def can_produce_phi(self):
        # Sanity check function
        return (len(self.seen_frame_buffer) == self.phi_length and
                len(self.compare_frame_buffer) == self.phi_length)

    def crop_frame(self, frame):
        # Apply per-game cropping to screens to get rid of useless info
        # NOTE: game screens are 210*160 by default
        # assume 'frame' var is a PIL.Image object
        if "Breakout" in self.env_name:
            # crop off score at the top and black space at bottom 
            # to make screen 179*160
            frame = frame.crop((0, 210-193, 160, 196))
        return frame
