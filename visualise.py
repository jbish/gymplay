import numpy as np
from PIL import Image
import sys

def main():
    filename = sys.argv[1]
    arr = np.load(filename)
    count = 1
    for phi in arr:
        for frame in phi:
            img = Image.fromarray(frame)
            img.save("./viz/{}.png".format(count), "PNG")
            count += 1

if __name__ == "__main__":
    main()
