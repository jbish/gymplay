import argparse
from pynput import keyboard
import gym
import sys
import numpy as np
from datetime import datetime
import preprocessing
import time
import signal
from gym.envs.classic_control import rendering
from scipy.misc import imresize

class GracefulKiller:

    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self,signum, frame):
        self.kill_now = True

# map keyboard presses to 
ACTION_SPACES = {
            "Breakout": {"Key.space": 1, "Key.right": 2, "Key.left": 3} # ['NOOP', 'FIRE', 'RIGHT', 'LEFT']
        }
capture_key = None # var to hold last captured key by keyboard listener

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("envname")
    parser.add_argument("--num-samples", type=int, default=5000)
    parser.add_argument("--phi-length", type=int, default=4)
    parser.add_argument("--frameskip", type=int, default=4)
    parser.add_argument("--disable-frame-maxing", action="store_true", default=False)
    parser.add_argument("--crop-screens", action="store_true", default=False)
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--framerate", type=int, default=10)
    return parser.parse_args()

def populate_preprocessor(env, obs, preprocessor, ep_frame_count, frameskip, phi_buildup_frames):
    # include first frame as seen/compare frame if necessary
    preprocessor.try_store_frame(ep_frame_count, frameskip, obs)
    init_action = env.action_space.sample() # random initial action
    prev_action = init_action # bootstrap prev action
    while ep_frame_count < phi_buildup_frames:
        # Select action
        action = None
        if ep_frame_count % frameskip == 0:
            action = env.action_space.sample() # pick new action
        else:
            action = prev_action # frameskipping
        prev_action = action
        # Perform action
        obs, _, _, _ = env.step(action)
        ep_frame_count += 1
        # Update preprocessor frames
        preprocessor.try_store_frame(ep_frame_count, frameskip, obs)
    assert preprocessor.can_produce_phi() # sanity check
    return obs, ep_frame_count

def save_dataset(phi_buffer, env_name): 
    phi_buffer = np.asarray(phi_buffer)
    np.save("./data/{}_{}.npy".format(env_name, datetime.now().strftime("%Y-%m-%d_%H:%M:%S")),
        phi_buffer)

def on_press(key):
    global capture_key
    capture_key = key 

def on_release(key):
    global capture_key
    capture_key = None # reset to nothing

def get_action(action_space, env_name):
    # translate last key press to action in env
    if capture_key is None:
        action = 0 # null op after key release
    else:
        try:
            action = ACTION_SPACES[env_name.split("-")[0]][str(capture_key)]
        except KeyError:
            # do nothing if action not recognised
            action = 0
    return action

def play(env, args):
    killer = GracefulKiller()
    print(env.unwrapped.get_action_meanings())
    # setup keyboard listener thread
    listener = keyboard.Listener(on_press=on_press, on_release=on_release)
    listener.start()
    phi_buffer = []
    viewer = rendering.SimpleImageViewer()
    while True:
        terminal = False
        compwise_max_flag = not args.disable_frame_maxing
        preprocessor = preprocessing.Preprocessor(args.phi_length, compwise_max_flag, 
            args.crop_screens, args.envname)
        obs = env.reset()
        ep_frame_count = 1
        # populate preprocessor with enough frames to make first phi, then continue with episode
        phi_buildup_frames = args.frameskip*args.phi_length
        obs, ep_frame_count = populate_preprocessor(env, obs, preprocessor, 
            ep_frame_count, args.frameskip, phi_buildup_frames)
        phi_buffer.append(preprocessor.create_phi())
        while not terminal:
            # resize screen
            rgb_arr = env.render("rgb_array")
            rgb_arr_resized = imresize(rgb_arr, 3.0)
            viewer.imshow(rgb_arr_resized)
            time.sleep(1/args.framerate)
            # check if user terminated game early
            if killer.kill_now:
                print("Saving partial data then exiting...")
                save_dataset(phi_buffer, args.envname)
                env.close()
                sys.exit(1) 
            # select new action
            action = get_action(env.action_space, args.envname)
            if ep_frame_count % args.frameskip == 0:
                phi_buffer.append(preprocessor.create_phi())      
                print("{}/{} phis collected".format(len(phi_buffer),
                    args.num_samples))
                # check if it's time to stop collecting data
                if len(phi_buffer) == args.num_samples:
                    print("Saving complete data then exiting...")
                    save_dataset(phi_buffer, args.envname)
                    env.close()
                    sys.exit(0)
            obs, _, terminal, _ = env.step(action)
            ep_frame_count += 1
            preprocessor.try_store_frame(ep_frame_count, args.frameskip, obs)

def main():
    args = parse_args()
    env = gym.make(args.envname)
    env.seed(args.seed)
    play(env, args)

if __name__ == "__main__":
    main()
