#!/bin/bash
#SBATCH --partition=batch
#SBATCH --nodes=1
source ~/virtualenvs/gymplay/bin/activate

python breakout_tracker_agent.py --disable-frame-maxing --crop-screens \
    --exp-discount=$1 --signal-cutoff=$2 --param-search
