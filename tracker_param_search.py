#!/usr/bin/python
import subprocess
import time
import numpy as np

def gen_param_vals():
    param_vals = {}
    param_vals["exp_discount"] = np.random.uniform(0.9, 1.0)  
    param_vals["signal_cutoff"] = np.random.uniform(0.0, 0.1)
    return param_vals

def main():
    jobs_to_run = 100
    job_limit = 8 
    while jobs_to_run > 0:
        jobs = subprocess.run(["squeue","-u","uqjbish3"], stdout=subprocess.PIPE).stdout.decode("utf-8")
        num_jobs = len(jobs.split("\n"))-1
        if num_jobs < job_limit:
           params = gen_param_vals()
           subprocess.Popen("sbatch tracker_param_search.sh {}"
               " {}".format(params["exp_discount"], params["signal_cutoff"]),
               shell=True)
           jobs_to_run -= 1
        time.sleep(1)

if __name__ == "__main__":
    main()
