#!/usr/bin/python
import argparse
import gym
import sys
import numpy as np
from datetime import datetime
import preprocessing
import time
from gym.envs.classic_control import rendering
from scipy.misc import imresize

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--num-samples", type=int, default=1000)
    parser.add_argument("--phi-length", type=int, default=4)
    parser.add_argument("--frameskip", type=int, default=4)
    parser.add_argument("--disable-frame-maxing", action="store_true", default=False)
    parser.add_argument("--crop-screens", action="store_true", default=False)
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--framerate", type=int, default=None)
    parser.add_argument("--exp-discount", type=float, default=0.95)
    parser.add_argument("--signal-cutoff", type=float, default=1/16)
    parser.add_argument("--param-search", action="store_true", default=False)
    return parser.parse_args()

def populate_preprocessor(env, obs, preprocessor, ep_frame_count, frameskip, phi_buildup_frames):
    # include first frame as seen/compare frame if necessary
    preprocessor.try_store_frame(ep_frame_count, frameskip, obs)
    init_action = 1 # hardcode fire
    prev_action = init_action # bootstrap prev action
    info = None
    while ep_frame_count < phi_buildup_frames:
        # Select action
        action = None
        if ep_frame_count % frameskip == 0:
            action = env.action_space.sample() # pick new action
        else:
            action = prev_action # frameskipping
        # Perform action
        obs, _, _, info = env.step(action)
        prev_action = action
        ep_frame_count += 1
        # Update preprocessor frames
        preprocessor.try_store_frame(ep_frame_count, frameskip, obs)
    assert preprocessor.can_produce_phi() # sanity check
    return obs, ep_frame_count, info

def save_dataset(phi_buffer, env_name): 
    phi_buffer = np.asarray(phi_buffer)
    np.save("./data/breakout_tracker/{}_{}.npy".format(env_name, datetime.now().strftime("%Y-%m-%d_%H:%M:%S")),
        phi_buffer)

def rolling_window(arr, size):
    shape = arr.shape[:-1] + (arr.shape[-1] - size + 1, size)
    strides = arr.strides + (arr.strides[-1],)
    return np.lib.stride_tricks.as_strided(arr, shape=shape, strides=strides)

def locate_ball(obs):
    # Ball top left corner is (63, 8), size of box is (126, 144). Box starts just
    # below top red row of bricks.
    # Colour of ball pixels is (200, 72, 72) RGB.
    # find first red pixel in area - this will locate the top left corner of
    # the ball
    rgb_rows = obs[63:(63+126),8:(160-8),:]
    ball_loc = None
    # aggregate over RGB axis (axis 3)
    indices = np.where(np.all(rolling_window(rgb_rows, 3) == [200, 72, 72],
        axis=3))
    # first array in indices is the row, second is the column. we care about
    # the column, so look at first index of that
    try:
        ball_loc = indices[1][0]
    except IndexError:
        pass # array empty so no ball found, just leave loc as None
    if ball_loc is not None:
        ball_loc = ball_loc/144 # normalise between 0 and 1
    return ball_loc

def locate_paddle(obs):
    # obs has shape 210*160*3
    # Paddle top left corner is (189, 8), size of box is (4, 144).
    # Colour of paddle pixels is (200, 72, 72) RGB.
    # Paddle is 16 pixels wide
    # Just scan the top row of the box region
    # find first pixel occurrence of red then add 7 to it to get to the center
    rgb_row = obs[189,8:(160-8),:]
    indices = np.where(np.all(rolling_window(rgb_row, 3) == [200, 72, 72],
        axis=2))
    paddle_left_loc = indices[0][0]
    paddle_center_loc = paddle_left_loc + 7
    return paddle_center_loc/144 # normalise between 0 and 1

def update_smoothed_vals(paddle_center_loc, ball_loc, paddle_loc_smooth,
        ball_loc_smooth, exp_discount):
    # delta rule to calc new estimate of position
    paddle_loc_smooth = paddle_loc_smooth + exp_discount*(paddle_center_loc - \
            paddle_loc_smooth)
    if ball_loc is None:
        ball_loc_smooth = None
    else:
        if ball_loc_smooth is None:
            ball_loc_smooth = ball_loc
        else:
            ball_loc_smooth = ball_loc_smooth + exp_discount*(ball_loc - \
                    ball_loc_smooth)
    return paddle_loc_smooth, ball_loc_smooth

def select_action(obs, paddle_loc_smooth, ball_loc_smooth, signal_cutoff,
        exp_discount):
    # select either 0, 2, 3 (no op, right, left) based on ball and paddle
    # position. Try to align centre of paddle with ball on x axis
    ball_loc = locate_ball(obs)
    paddle_center_loc = locate_paddle(obs)
    paddle_loc_smooth, ball_loc_smooth = \
        update_smoothed_vals(paddle_center_loc, ball_loc, paddle_loc_smooth,
            ball_loc_smooth, exp_discount)
    action = 0
    if ball_loc_smooth is not None:
        error = ball_loc_smooth - paddle_loc_smooth
        signal = error
        if signal > signal_cutoff:
            action = 2 # right
        elif signal < -1*signal_cutoff:
            action = 3 # left
    return action, paddle_loc_smooth, ball_loc_smooth

def run_episode_no_phi_buffer(env, args):
    terminal = False
    compwise_max_flag = not args.disable_frame_maxing
    preprocessor = preprocessing.Preprocessor(args.phi_length, compwise_max_flag, 
        args.crop_screens, "Breakout-v0")
    obs = env.reset()
    ep_frame_count = 1
    # populate preprocessor with enough frames to make first phi, then continue with episode
    phi_buildup_frames = args.frameskip*args.phi_length
    obs, ep_frame_count, info = populate_preprocessor(env, obs, preprocessor, 
        ep_frame_count, args.frameskip, phi_buildup_frames)
    paddle_loc_smooth = locate_paddle(obs)
    ball_loc_smooth = locate_ball(obs)
    curr_lives = info["ale.lives"]
    fire_counter = 0
    ep_return = 0
    while not terminal:
        # limit framerate if necessary
        if args.framerate is not None:
            time.sleep(1/args.framerate)
        # select new action
        action = None
        new_lives = info["ale.lives"]
        if fire_counter > 0:
            action = 1
            # reduce fire counter
            fire_counter -= 1
        elif new_lives == curr_lives - 1:
            action = 1
            # reset fire counter
            fire_counter = args.frameskip - 1
        else:
            action, paddle_loc_smooth, ball_loc_smooth = select_action(obs,
                paddle_loc_smooth, ball_loc_smooth, args.signal_cutoff,
                args.exp_discount)
        curr_lives = info["ale.lives"]
        obs, reward, terminal, info = env.step(action)
        ep_return += reward
        ep_frame_count += 1
        preprocessor.try_store_frame(ep_frame_count, args.frameskip, obs)
    print("Return: {}".format(ep_return))
    return ep_return

def run_episode(env, args, phi_buffer):
    viewer = rendering.SimpleImageViewer()
    terminal = False
    compwise_max_flag = not args.disable_frame_maxing
    preprocessor = preprocessing.Preprocessor(args.phi_length, compwise_max_flag, 
        args.crop_screens, "Breakout-v0")
    obs = env.reset()
    ep_frame_count = 1
    # populate preprocessor with enough frames to make first phi, then continue with episode
    phi_buildup_frames = args.frameskip*args.phi_length
    obs, ep_frame_count, info = populate_preprocessor(env, obs, preprocessor, 
        ep_frame_count, args.frameskip, phi_buildup_frames)
    phi_buffer.append(preprocessor.create_phi())
    paddle_loc_smooth = locate_paddle(obs)
    ball_loc_smooth = locate_ball(obs)
    curr_lives = info["ale.lives"]
    fire_counter = 0
    ep_return = 0
    while not terminal:
        # resize screen
        rgb_arr = env.render("rgb_array")
        rgb_arr_resized = imresize(rgb_arr, 3.0)
        viewer.imshow(rgb_arr_resized)
        # limit framerate if necessary
        if args.framerate is not None:
            time.sleep(1/args.framerate)
        # select new action
        action = None
        new_lives = info["ale.lives"]
        if fire_counter > 0:
            action = 1
            # reduce fire counter
            fire_counter -= 1
        elif new_lives == curr_lives - 1:
            action = 1
            # reset fire counter
            fire_counter = args.frameskip - 1
        else:
            action, paddle_loc_smooth, ball_loc_smooth = select_action(obs,
                paddle_loc_smooth, ball_loc_smooth, args.signal_cutoff,
                args.exp_discount)
        if ep_frame_count % args.frameskip == 0:
            phi_buffer.append(preprocessor.create_phi())      
            print("{}/{} phis collected".format(len(phi_buffer),
                args.num_samples))
            # check if it's time to stop collecting data
            if len(phi_buffer) == args.num_samples:
                print("Saving complete data then exiting...")
                save_dataset(phi_buffer, "Breakout-v0")
                env.close()
                sys.exit(0)
        curr_lives = info["ale.lives"]
        obs, reward, terminal, info = env.step(action)
        ep_return += reward
        ep_frame_count += 1
        preprocessor.try_store_frame(ep_frame_count, args.frameskip, obs)
    print("Return: {}".format(ep_return))
    return phi_buffer

def play(env, args):
    if args.param_search:
        ep_returns = []
        for i in range(10):
            ep_return = run_episode_no_phi_buffer(env, args)
            ep_returns.append(ep_return)
        print("exp_discount: {}, signal_cutoff: {}, min_return: {},"
                " max_return: {}, avg_return: {}".format(args.exp_discount,
                    args.signal_cutoff, np.min(ep_returns),
                    np.max(ep_returns), np.mean(ep_returns)))
    else:
        phi_buffer = []
        while True:
            phi_buffer = run_episode(env, args, phi_buffer)

def main():
    args = parse_args()
    env = gym.make("Breakout-v0")
    env.seed(args.seed)
    # gym action space sampling uses np internally, so seed np same as env
    np.random.seed(args.seed)
    play(env, args)

if __name__ == "__main__":
    main()
